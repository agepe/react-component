import React from 'react';
import ReactDOM from "react-dom";
import faker from 'faker';
import CommentDetail from './CommentDetail';
import ApprovalCard from './ApprovalCard';

const App = () => {
    return (
        <div className="ui container comments">
            <ApprovalCard>
                <CommentDetail author="Ronaldo" timeComment="Today at 5:00PM" avatar={faker.image.abstract()} commentContent="Nice Gan!"/>
            </ApprovalCard>
            <ApprovalCard>
                <CommentDetail author="Messi" timeComment="Today at 1:00PM" avatar={faker.image.people()} commentContent="Mantap betool!"/>
            </ApprovalCard>
            <ApprovalCard>
                <CommentDetail author="Rashford"  timeComment="Today at 4:00AM" avatar={faker.image.cats()} commentContent="Cus Ahh!"/>
            </ApprovalCard>
        </div>

    );
};

ReactDOM.render(<App />, document.querySelector('#root'));